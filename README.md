
# **Curso IALAB - Start Coding - JavaScript**

Curso de Inteligencia Artificial - Start Coding - JavaScript.

## **Unidad 1: Basics**

1. Course Introduction
2. Intro to Javascript
3. Linking a Javascript File
4. Code Editor and Tools
5. Values and Types
6. Values and Types (cont)
7. Expressions and Statements
8. Type manipulation
9. Type Manipulation (cont)
10. Operators
11. Operators (cont)
12. Operators precedence
13. Var, let and const
14. Template Literals
15. Exercises Module 1
16. Repositorio de código del curso

## **Unidad 2: Flow Control**

1. If/else
2. Switch
3. For
4. While y do while
5. Exercises Flow Control
6. Coding Challenge - Part I
7. Coding Challenge - Part II
8. Coding Challenge - Part III
9. Coding Challenge - Part IV

## **Unidad 3: Functions**

1. Intro to functions
2. Functions structure
3. Scope
4. Hoisting
5. Coding Challenge - Part I
6. Coding Challenge - Part II
7. Coding Challenge - Part III
8. Coding Challenge - Part IV
9. Truthy & Falsy

## **Unidad 4: Arrays**

1. Intro to Arrays
2. Array manipulation
3. Array manipulation (cont)
4. Array manipulation (cont)
5. Array manipulation (cont)
6. Array manipulation (cont)
7. Exercises

## **Unidad 5: Objects**

1. Intro to Objects
2. POO - Classes
3. Extend
4. Object Manipulation
5. Exercises
6. Exercises (cont)

## **Unidad 6: Asynchrony**

1. Intro
2. Call stack
3. setTimeout Examples
4. Promises
5. Async-await
6. Fetch
7. Examples: Promises and fetch
8. Examples: Promises and fetch (cont)
9. Exercises (Part I)
10. Exercises (Part II)
11. Exercises (Part III)
12. JSON
13. HTTP Methods: GET and POST
14. HTTP Methods: PUT and DELETE

## **Unidad 7: DOM**

1. Intro to DOM
2. Manipulating nodes
3. Manipulating styles
4. Create and delete nodes
5. Exercises: DOM

## **Unidad 8: Events**

1. Intro to Events
2. Event Object
3. Listening Events and Modifying DOM in response
4. Listening Events and Modifying DOM in response (cont)
5. Mousemove event
6. Finishing Grid Paint project
7. Coding Challenge: Events
8. Coding Challenge: Events (cont)

## **Unidad 9: Extras**

1. Destructuring
2. Spread Operator
3. Functional Programing
4. Intro to libraries

## **Tutoría Sincrónica**

* Primera tutoría sincrónica: Start Coding
