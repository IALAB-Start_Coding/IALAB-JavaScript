// 1) Pedir al usuario que ingrese su nombre. Guardar ese nombre en una variable y utilizarlo para saludar al usuario ("Hola Alexis").
/*
const nombre = prompt("ingresa tu nombre");
console.log(`Hola ${nombre}`);
*/
// 2) Pedirle a un usuario que ingrese dos numeros y devolverle la suma de dichos numeros.
/*
const numberOne = prompt("ingresa el primer numero");
const numberTwo = prompt("ingresa el segundo numero");
console.log(Number(numberOne) + Number(numberTwo));
*/
// 3) Pedirle a un usuario que ingrese dos numeros y devolverle el doble de la suma de dichos numeros.
/*
const numberOne = prompt("ingresa el primer numero");
const numberTwo = prompt("ingresa el segundo numero");
console.log((Number(numberOne) + Number(numberTwo)) * 2);
*/
// 4) Pedirle al usuario que ingrese el ancho y el alto de una habitacion y calcular la superficie.
/*
const numberOne = prompt("ingresa el ancho");
const numberTwo = prompt("ingresa el alto");
console.log(Number(numberOne) * Number(numberTwo) + " de superficie.");
*/
// 5) Pedirle al usuario que ingrese su nombre y devolverle el nombre todo en miniscula.
/*
const nombre = prompt("ingresa tu nombre");
console.log(nombre.toLowerCase());
*/
// 6) Pedirle al usuario que ingrese su apellido y devolverle todo en mayuscula.
/*
const lastname = prompt("ingresa tu apellido");
console.log(lastname.toUpperCase());
*/
/*
7) Pedirle al usuario el nombre, el apellido y la edad. Mostrar el siguiente mensaje:
    Nombre: Diego
    Apellido: Maradona
    Edad: 10
*/
/*
const firstname = prompt("ingresa tu nombre");
const lastname = prompt("ingresa tu apellido");
const age = prompt("ingresa tu edad");
alert(`
  Nombre: ${firstname}
  Apellido: ${lastname}
  Edad: ${age}
`);
*/
