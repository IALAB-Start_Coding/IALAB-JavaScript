// ********** VALORES Y TIPOS **********

//Number
console.log(3);
console.log(typeof 3.5);
console.log(typeof Infinity);
console.log(typeof NaN);

//Strings
console.log(typeof "Hola Mundo");
console.log(typeof "3");

//Booleanos
console.log(typeof true);
console.log(typeof false);

//Null y Undefined
console.log(undefined);
console.log(null);

let miVariable;
console.log(miVariable);

miVariable = null;
console.log(miVariable);

miVariable = 3;
console.log(miVariable);

// ********** EXPRESIONES vs DECLARACIONES **********

console.log("Lo que sigue es una expresion porque lo que retorna es un valor");

let miVar = 3;
console.log(miVar);

console.log(2 + 3);

function saludar() {
  return "Hola";
}
console.log(saludar());

console.log("Lo que sigue es una declaracion porque no retorna un valor");

let miVar2 = false;
if (miVar2 == true) {
  console.log("Verdadero");
} else {
  console.log("Falso");
}

// ********** METODOS NAVEGADOR **********

alert("Hello world");

let nombre = prompt("ingresa tu nombre");
console.log(nombre);

console.log(confirm("Estas seguro?"));

// ********** MANIPULACION DE TIPOS **********

// Valores literales
let valorLit = "Alexis";
console.log(valorLit);

// Conversion de string a Number

//Number, parseInt, +
let string = "12tres4";
console.log(Number(string));
console.log(parseInt(string));
console.log(+string);

//Concatenacion
let firstname = "Javier";
console.log("Hola" + " " + firstname);
console.log("hola Javier");

// Conversion a Booleanos
let caracter = "2";
let numero = 0;

console.log(Boolean(caracter));
console.log(Boolean(numero));

console.log(!!caracter);
console.log(!!numero);

// Conversion implicita
let char = "2";
let num = 2;
let otronum = 3;

console.log(num + otronum + char);
console.log(char + num + otronum);
console.log(otronum + char + num);

// ********** OPERADORES **********

// Aritmeticos

// Unarios (-, +, typeof)
console.log(typeof "Hola mundo");
console.log(+"123");
console.log(-"123");
console.log(-"tres");

// Binarios
console.log(2 + 3);
console.log(3 + 3);
console.log(2 * 6);
console.log(6 / 3);
console.log(6 % 3);

// Logicos

// Binarios

// Mayor y menor
console.log(2 > 3);
console.log(2 < 3);
console.log(3 <= 3);
console.log(3 <= 3);

// === (Extricta. Compara el tipo y el valor.) y ==
console.log(2 === "2");
console.log(3 == "3");
console.log(2 === 3);
console.log(2 == 3);

console.log(3 !== "3");
console.log(3 != "3");

// AND, OR, NOT

// AND
console.log(2 === 2 && 2 > 0);
console.log(2 === "2" && 2 > 0);
console.log(2 === 2 && 2 < 0);
console.log(2 === "2" && 2 < 0);

//OR
console.log(2 === 2 || 2 > 0);
console.log(2 === "2" || 2 > 0);
console.log(2 === 2 || 2 < 0);
console.log(2 === "2" || 2 < 0);

// Ternario
let edad = 17;
console.log(edad >= 18 ? "Podes manejar" : "No tenes edad para manejar");

// Operator Precedence
console.log((30 + 20) / 2); // Associativity - left-to-right
let last = "Maradona"; // Associativity - right-to-left

let x, y, z;
console.log(x, y);

x = y = 10;
console.log(x, y);

x = z;
y = 10;
console.log(z, y);

// ********** VAR, LET and CONST **********
// palabras reservadas para declarar una variable : var, let y conts
// La diferencia lo veremos mas adelante, pero para adelantar. Tiene que ver con el Hoisting.

var miPrimerVariable = 1;
let miSegundaVariable = 2; // Nos permite reasignar
const miTercerVariable = 3; // No nos permite reasignar/cambiar el valor

console.log(miPrimerVariable);
console.log(miSegundaVariable);
console.log(miTercerVariable);

// ********** TEMPLATE LITERALS **********
const firstName = "Diego";
const job = "Developer";
const favouriteNumber = 7;

// Hola, soy Diego, soy Developer
// y mi numero favorito es el 7

//sin template literal
console.log(
  "Hola, soy" +
    firstName +
    ". soy" +
    job +
    "y mi numero favorito es el" +
    favouriteNumber
);
// con template literals
console.log(
  `Hola, soy ${firstName}, 
  soy ${job} y 
  mi numero favorito es el ${favouriteNumber}`
);
