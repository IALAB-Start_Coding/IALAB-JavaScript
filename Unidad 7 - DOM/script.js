// ********** DOM **********

// Tenemos 3 formas de acceder a los nodos.
// Por el nombre de la etiqueta/tag. Ej.: header - body - main.
// Por el nombre de la clase. Ej. container.
// Por el nombre de la id. Ej. Avatar.

// Acceder a un nodo.

// 1) Por el tag name.
const header = document.getElementsByTagName("header"); //Nos trae todas los elemento con el nombre "header"
console.log(header);

// 2) Por el nombre de la clase.
const container = document.getElementsByClassName("container"); //Nos trae todas los elemento con el nombre "container"
console.log(container);

// 3) Por ID
const avatar = document.getElementById("avatar"); // Trae el primero que encuentra el elemento.
console.log(avatar);

// 4) QuerySelector
const main = document.querySelector("main"); //nombre del tag
console.log(main);
const username = document.querySelector(".username"); //nombre de clase
console.log(username);
const repos = document.querySelector("#repos"); // id
console.log(repos);

// 4) QuerySelectorAll
const usernames = document.querySelectorAll(".username"); //nombre de clase
console.log(usernames);

// ********** MANIPULAR ELEMENTOS **********

const usernameEdit = document.querySelector(".username"); // id
usernameEdit.textContent = "Javier"; // 1° forma de modificar
console.log(usernameEdit.textContent);
usernameEdit.innerText = "Diego"; // 2° forma de modificar

// Acceder y modificar src de img
const avat = document.querySelector("#avatar");
avat.src = "https://i.pravatar.cc/300";
console.log(avat);

// Incorporar HTML mediante JS
const list = document.querySelector("#repos-list");
list.innerHTML = `
<li> Un Repo </li>
<li> Otro <strong>Repo</strong> </li>
`;

console.log(list.outerHTML); // Retorna todo el elemento.
console.log(list.innerHTML); // Retorna lo que hay dentro

// Modificar estilos
// Esto sobreescribe la clase
// const contain = document.querySelector(".container");
// console.log(contain.className);
// contain.className = "background-black";
// console.log(contain.className);

// Esto agrega la clase
const contain = document.querySelector(".container");
console.log(contain.classList);
contain.classList.add("background-black");
console.log(contain.classList);

// Elimina la clase
contain.classList.remove("background-black");
console.log(contain.classList);

// Toggle - Agrega o quita (swithea) la clase al elemento.
const swithTheme = () => {
  const container = document.querySelector(".container");
  const textNodes = document.querySelectorAll(".text"); //nombre de clase
  const avatar = document.querySelector("#avatar");

  container.classList.toggle("background-black");
  textNodes.forEach((element) => element.classList.toggle("white-text"));

  if (container.classList.contains("background-black")) {
    avatar.style.border = "2px solid red";
  } else {
    avatar.style.border = "2px solid black";
  }
};

swithTheme();

// ********** CREATE AND DELETE NODES **********

// Eliminar un nodo
const reposs = document.querySelector("#repos");
reposs.remove();

// Agregar un nodo (al final)
const reposNode = document.createElement("h2"); // creo al hijo
reposNode.textContent = "Repos";
reposNode.classList.add("text", "white-text");
console.log(reposNode);
const parentAside = document.querySelector("aside"); // me traigo al padre
parentAside.appendChild(reposNode); // Agrego el hijo al padre

// before (antes) y after (despues)
// const reposList = document.querySelector("#repos-list");
// reposList.before(reposNode);

// Link de propiedades y metodos de elementos => https://developer.mozilla.org/es/docs/Web/API/Element
// link de propieades y metodos document => https://developer.mozilla.org/es/docs/Web/API/Document
