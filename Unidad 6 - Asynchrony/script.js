// ********** ASINCRONICO **********

/*
console.log("Me despierto");
console.log("Me lavo la cara");
console.log("Pongo el cafetera");
for (let time = 120; time >= 0; time--) {
  console.log(time);
  if (time === 0) console.log("El cafe esta listo");
}
console.log("Desayuno");
console.log("Me siento a trabajar");
*/

// setTimeOut()
/*
console.log("Me despierto");
console.log("Me lavo la cara");
console.log("Pongo el cafetera");
setTimeout(() => {
  console.log("El cafe esta listo");
}, 5000);
console.log("Me doy uns ducha");
*/
// ASINCRONISMO Y CALL STACK

// EJEMPLO 1
/*
console.log("Mario");
console.log("Juan");
console.log("Pedro");
*/
// EJEMPLO 2
/*
setTimeout(() => console.log("Mario"), 1000);
console.log("Juan");
console.log("Pedro");
*/
// EJEMPLO 3
/*
setTimeout(() => console.log("Mario"), 0);
console.log("Juan");
console.log("Pedro");
*/
// EJEMPLO 4
/*
setTimeout(() => console.log("Mario"), 0);
for (let index = 0; index <= 5000; index++) {
  if (index === 5000) console.log("Juan");
}
console.log("Pedro");
*/

// ********** PROMESAS **********

/*
function pagarConTrajeta(precio) {
  const limite = 10000;
  console.log("Conectando...");
  setTimeout(() => {
    if (precio < limite) console.log("Compra Aprobada");
    else console.log("Compra rechazada");
  }, 2000);
}

pagarConTrajeta(100000);
*/

// La promesa tiene 3 estados: pendiente (nunca se resuelve), resuelta y rechazada (lo tenemos disponible).
// Puede ser que lo tengamos disponible en un futuro
/*
const pagarConTrajeta = (precio, isConected) =>
  new Promise((resolve, reject) => {
    const limite = 10000;
    if (!isConected) return;
    console.log("Procesando el pago");

    setTimeout(() => {
      if (precio <= limite) resolve("Compra Aprobada");
      else reject("Compra Rechazada");
    }, 3000);
  });

console.log(
  pagarConTrajeta(100000, false)
    .then((respuesta) => {
      console.log(respuesta);
    })
    .catch((error) => console.log(error))
);

console.log("Otra tarea");
*/

// ********** ASYNC-AWAIT **********
/*
const pagarConTarjeta = (precio, isConected) =>
  new Promise((resolve, reject) => {
    const limite = 10000;
    if (!isConected) return;
    console.log("Procesando el pago");

    setTimeout(() => {
      if (precio <= limite) resolve("Compra Aprobada");
      else reject("Compra Rechazada");
    }, 3000);
  });

// Otra alternativa de then y catch es async
async function procesarPago() {
  try {
    const respuesta = await pagarConTarjeta(100000, true);
    console.log(respuesta);
  } catch (error) {
    console.log(error);
  }
}
// Tambien se puede con arrow function
const procesarPagoArrowFunction = async () => {
  try {
    const respuesta = await pagarConTarjeta(100000, true);
    console.log(respuesta);
  } catch (error) {
    console.log(error);
  }
};
//procesarPago();

// Promise.all
const allPromises = [
  pagarConTarjeta(100, true),
  pagarConTarjeta(100, true),
  pagarConTarjeta(1000, true),
];
async function procesarPagos() {
  try {
    const respuesta = await Promise.all(allPromises);
    console.log(respuesta);
    return "Todas resueltas";
  } catch (error) {
    console.log(error);
  }
}
console.log(procesarPagos().then((response) => console.log(response))); // una funcion async, en si es una promesa y si quiero manejar sus respuestas entonces uso then-catch o try-catch
*/
// FETCH
/*
const getDataFromApi = async () => {
  try {
    const response = await fetch(
      "https://jsonplaceholder.typicode.com/todos/1"
    );
    const jsonResponse = await response.json();
    return jsonResponse;
  } catch (error) {
    console.log("error => ", error);
  }
};

console.log(getDataFromApi().then((response) => console.log(response)));
*/
// ********** PROMISES AND FETCH **********

// EJEMPLO 1
/*
const asyncOperation = new Promise((resolve) => resolve());
asyncOperation.then((resolvedValue) => console.log("Mario"));
console.log("Juan");
console.log("Pedro");
*/
// EJEMPLO 2
/*
const asyncOperation = fetch("https://jsonplaceholder.typicode.com/todos/1");
asyncOperation.then((resolvedValue) => console.log("Mario"));
console.log("Juan");
console.log("Pedro");
*/
// EJEMPLO 3
/*
async function fetchSomething() {
  await new Promise((resolve) => {
    setTimeout(() => {
      console.log("Mario");
      resolve();
    }, 10);
  }); // No imprime Juan hasta que termine el bloque de await.

  console.log("Juan");
}

fetchSomething();

console.log("Pedro");
*/
// EJEMPLO 4
/*
setTimeout(() => console.log("Mario"), 5);

const promise = new Promise((resolve) => {
  setTimeout(() => resolve(), 10);
});

promise.then(() => console.log("Juan"));
console.log("Pedro");
*/
// EJEMPLO 5
/*
setTimeout(() => console.log("Mario"), 0);
const promise = Promise.resolve(); // entre el setTimeout y una promesa, se resuelve la promesa primero.
promise.then(() => console.log("Juan"));
console.log("Pedro");
*/

// ********** JSON **********
/*0.
const body = {
  id: 1,
  title: "foo",
  body: "bar",
  userId: 1,
};

console.log(typeof body);

// Convertir a JSON
const parsedBody = JSON.stringify(body);

console.log(parsedBody);
console.log(typeof parsedBody);

// Convertir de JSON a JS

const bodyToJS = JSON.parse(parsedBody);
console.log(bodyToJS);
*/

// ********** PROTOCOLO HTTP **********

// GET, POST, PUT, DELETE

// GET => Solicitar datos o informacion al servidor
// POST => Enviar informacion al servidor
// PUT => Modificar informacion en el servidor.
// DELETE => Borrar informacion del servidor.

//GET
// fetch("https://jsonplaceholder.typicode.com/posts/10")
//   .then((response) => response.json())
//   .then((json) => console.log(json));

// fetch("https://jsonplaceholder.typicode.com/posts")
//   .then((response) => response.json())
//   .then((json) => console.log(json));

//POST
// fetch("https://jsonplaceholder.typicode.com/posts", {
//   method: "POST",
//   body: JSON.stringify({
//     title: "foo",
//     body: "bar",
//     userId: 1,
//   }),
//   headers: {
//     "Content-type": "application/json; charset=UTF-8",
//   },
// })
//   .then((response) => response.json())
//   .then((json) => console.log(json));

//PUT
// fetch("https://jsonplaceholder.typicode.com/posts/1", {
//   method: "PUT",
//   body: JSON.stringify({
//     id: 1,
//     title: "foo",
//     body: "bar",
//     userId: 1,
//   }),
//   headers: {
//     "Content-type": "application/json; charset=UTF-8",
//   },
// })
//   .then((response) => response.json())
//   .then((json) => console.log(json));

//PATCH => Actualiza parcialmente el recurso
// fetch("https://jsonplaceholder.typicode.com/posts/1", {
//   method: "PATCH",
//   body: JSON.stringify({
//     title: "foo",
//   }),
//   headers: {
//     "Content-type": "application/json; charset=UTF-8",
//   },
// })
//   .then((response) => response.json())
//   .then((json) => console.log(json));

//DELETE
// fetch("https://jsonplaceholder.typicode.com/posts/1", {
//   method: "DELETE",
// });
