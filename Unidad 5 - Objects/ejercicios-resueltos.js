// 1)
class Sport {
  constructor(name, isTeamSport) {
    this.name = name;
    this.isTeamSport = isTeamSport;
  }

  sayName() {
    console.log(`El nombre del deporte es ${this.name}`);
  }

  sayIsTeamSport() {
    const message = this.isTeamSport
      ? `${this.name} es un deporte de equipo`
      : `${this.name} no es un deporte de equipo`;

    console.log(message);
  }
}

class TeamSports extends Sport {
  constructor(name, playersByTeam) {
    super(name);
    this.playersByTeam = playersByTeam;
  }

  sayPlayersByTeam() {
    console.log(
      `Para jugar al ${this.name} necesitas ${this.playersByTeam} jugadores por equipo`
    );
  }
}

const tenis = new Sport("Tenis", false);
tenis.sayName();
tenis.sayIsTeamSport();

const futbol = new TeamSports("Futbol", 11);

futbol.sayName();
futbol.sayPlayersByTeam();

const basket = new TeamSports("Basket", 5);

basket.sayName();
basket.sayPlayersByTeam();

// 2)
const AndreaBag = { cervezas: 2, sandwiches: 2, papasFritas: 1 };
const MartinBag = { torta: 1, gaseosa: 1, galletitas: 1 };

// console.log(AndreaBag, MartinBag);

const AndreaProducts = Object.keys(AndreaBag);
const MartinProducts = Object.keys(MartinBag);

// console.log(AndreaProducts, MartinProducts);

Object.assign(MartinBag, AndreaBag);

// console.log(AndreaBag);

// console.log(MartinBag);

//usar Object.keys

const productsName = Object.keys(MartinBag);

// console.log(productsName);

let totalProducts = 0;

// productsName.forEach((product) => (totalProducts += MartinBag[product]));

// console.log(totalProducts);

// for (const key in MartinBag) {
//   if (Object.hasOwnProperty.call(MartinBag, key)) {
//     totalProducts += MartinBag[key];
//   }
// }

// console.log(totalProducts);

const productsQuantity = Object.values(MartinBag);

const totalProductsWithReduce = productsQuantity.reduce(
  (acumulador, currentValue) => acumulador + currentValue,
  0
);

console.log(totalProductsWithReduce);
