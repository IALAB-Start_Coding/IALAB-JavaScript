// ********** OBJECTS **********
// {key: value}
// {firstname: "Diego"}

// FORMAS DE DECLARAR UN OBJETO
const literalObject = {
  firstname: "Diego",
  lastname: "Maradona",
  "city-residence": "Buenos Aires",
};
console.log(literalObject);

const normalObject = new Object();
console.log(normalObject);

normalObject.firstname = "Diego";
console.log(normalObject);

// AGREGANDO PROPIEDAD A UN OBJETO
literalObject.favoiriteColor = "Verde";
console.log(literalObject);

// ACCEDIENDO AL VALOR
console.log(literalObject["firstname"]);
console.log(literalObject.lastname);
console.log(literalObject["city-residence"]);

// modificar el valor de una propiedad
literalObject.favoiriteColor = "Azul";
console.log(`Nuevo color ${literalObject.favoiriteColor}`);

// POO - CLASSES
class Persona {
  constructor(firstname, city, job) {
    this.firstname = firstname;
    this.city = city;
    this.job = job;
  }
  // metodo dentro de la clase
  saludar() {
    console.log(`Hola soy ${this.firstname}`);
  }
}
const Javier = new Persona("Javier", "Buenos Aires", "Developer");
const Pedro = new Persona("Pedro", "San pablo", "Arquitecto");
const Lucas = new Persona("Lucas", "Santiago", "Artista");

console.log(Javier, Pedro, Lucas);

Javier.saludar();
Pedro.saludar();

// Extender una subclase desde una clase original
class Animal {
  constructor(name, isDomestic) {
    this.name = name;
    this.isDomestic = isDomestic;
  }

  speak() {
    console.log(`${this.name} hace un ruido`);
  }
}

class Dog extends Animal {
  constructor(name, age, isDomestic) {
    super(name, isDomestic);
    this.age = age;
  }

  speak() {
    console.log(
      `${this.name} ladra y ${this.isDomestic ? "es domestico" : "es salvaje"}`
    );
  }

  sayAge() {
    console.log(`${this.name} tiene ${this.age}`);
  }
}

const dog = new Dog("Rulo", 5, false);
dog.speak();
dog.sayAge();

// MANIPULACION DE OBJETOS
const person = {
  firstname: "Javier",
  city: "Buenos Aires",
  job: "Developer",
};

// Modificar un elemento
person.city = "Cordoba";
console.log(person.city);

// Object.key()
const keys = Object.keys(person);
console.log(keys);
// Iterar sobre cada key con ForEach
keys.forEach((key) => console.log(person[key]));

// For in
for (const key in person) {
  if (Object.hasOwnProperty.call(person, key)) {
    console.log(person[key]);
  }
}

// Object.values()
const values = Object.values(person);
console.log(values);
values.forEach((value) => console.log(value));

// Object.assign()
const developer = {
  skills: "Javascript",
  city: "Buenos Aires",
};
Object.assign(person, developer);
const modifiedPerson = { ...person, ...developer }; // Otra forma mergear objetos: spread operator u operador de propagación
console.log(person);
console.log(modifiedPerson);

// Object.entries()
const entries = Object.entries(person);
console.log(entries);
