// 1)
/*
acum = "";
for (let i = 1; i <= 5; i++) {
  acum = acum + "#";
  console.log(acum);
}
*/
// 2)
/*
for (let i = 0; i <= 10; i++) {
  console.log(i % 2 === 0 ? `${i} es par` : `${i} es impar`);
}
*/
// 3)
/*
for (let i = 1; i <= 15; i++) {
  if (i % 3 === 0 && i % 5 === 0) {
    console.log(`${i} - FizzBuzz`);
  } else if (i % 3 === 0) {
    console.log(`${i} - Fizz`);
  } else if (i % 5 === 0) {
    console.log(`${i} - Buzz`);
  } else {
    console.log(`${i}`);
  }
}
*/

// CODING CHALLENGE

// 1) REGISTRO.

let isRegistered, registerUserName, registerPassword;

const wishesToRegister = confirm(
  "Bienvenido al sitio. Haz click en OK para registrarte"
);

do {
  if (wishesToRegister) {
    if (!registerUserName) {
      registerUserName = prompt("Ingresa tu nombre de usuario");
    }

    if (registerUserName.length >= 3) {
      registerUserName = registerUserName.toLowerCase();

      registerPassword = prompt("Ingresa tu password");

      if (registerPassword.length >= 6) {
        alert("El registro se completo exitosamente");
        isRegistered = true;
      } else {
        alert("El password tiene que tener por lo menos 6 caracteres");
        registerPassword = null;
      }
    } else {
      alert("El nombre de usuario tiene que tener por lo menos 3 caracteres");
    }
  } else {
    alert("OK, te esperamos cuando gustes");
  }
} while (wishesToRegister && (!registerPassword || !registerUserName));

// 2) LOGIN

let loginPassword, loginUsername;

if (isRegistered) {
  let wishesToLogin = confirm("Haz click en OK para iniciar sesion");

  do {
    if (wishesToLogin) {
      loginUsername = prompt("Ingresa tu usuario");

      if (loginUsername !== null) {
        loginUsername = loginUsername.toLowerCase();
        loginPassword = prompt("Ingresa tu password");

        if (loginPassword === null) {
          wishesToLogin = false;
        } else if (
          loginUsername === registerUserName &&
          loginPassword === registerPassword
        ) {
          alert("Login Exitoso!");
        } else {
          alert("Alguno de los datos ingresados es incorrecto");
          loginPassword = null;
          loginUsername = null;
        }
      } else {
        wishesToLogin = false;
      }
    } else {
      alert("Vuelve cuando gustes");
    }
  } while (wishesToLogin && (!loginUsername || !loginPassword));
}

// 3) EXTRA

// Ver 1) y 2)
