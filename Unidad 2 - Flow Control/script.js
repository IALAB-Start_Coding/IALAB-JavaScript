// ********** CONTROL DE FLUJO **********

// IF/ELSE
let age = 18;

if (age >= 18) {
  console.log(age >= 18);
  console.log("Podes manejar");
} else {
  console.log(age >= 18);
  console.log("No podes manejar");
}

// ELSE IF
let number = 10;

if (number < 10) {
  console.log("El numero es menor a 10");
} else if (number > 10) {
  console.log("El numero es mayor a 10");
} else if (number === 10) {
  console.log("El numero es igual a 10");
} else {
  console.log("El valor ingresado no es un nemero");
}

// SWITCH
const weather = "SOLEAdo";

switch (weather.toUpperCase()) {
  case "FRESCO":
    console.log("Lleva abrigo");
    break;
  case "LLUVIOSO":
    console.log("Lleva paragua");
    break;
  case "SOLEADO":
    console.log("Cuidate del sol");
    break;
  default:
    console.log("El tiempo esta bueno. Disfruta del dia");
    break;
}

// FOR
for (let i = 1; i <= 5; i++) {
  console.log(i);
} // i+1 es igual a i++

// WHILE
let numberWhile = 1;

while (numberWhile <= 5) {
  console.log(numberWhile);
  numberWhile++;
}

// DO WHILE
let numberDoWhile = 1;

do {
  console.log("con do while", numberDoWhile);
  numberDoWhile++;
} while (numberDoWhile < 1);
