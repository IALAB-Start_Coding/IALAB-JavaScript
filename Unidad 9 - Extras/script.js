// ********** EXTRAS **********
const names1 = ["Alexis", "Juan", "Pedro"];
const names2 = ["Martin", "Javier", "Matias"];

const person1 = {
  firstname: "Juan",
  job: "Contador",
  city: "Cordoba",
};

// ********** DESTRUCTURING - DESARMAR **********

// En arreglos

// Sin destructuring
// const user1 = names1[0];
// const user2 = names1[1];
// const user3 = names1[2];
// console.log(user1, user2, user3);

// Con destructuring
// const [user1, user2, user3] = names1;
// console.log(user1, user2, user3);

// En objetos

// Sin destructuring
// const user1 = person1.firstname;
// const job = person1.job;
// console.log(user1, job);

// Con destructuring
// const { firstname, job: trabajo } = person1;
// console.log(firstname, trabajo);

// ********** SPREAD OPERATOR **********

// En arreglos

// 1) concatenar arrays (concat)
// const margedArray = [...names1, ...names2];
// console.log(margedArray);

// 2) Copia de arrays
// const copiedArray = [...names1];
// copiedArray.push("Marcelo");
// console.log(names1);
// console.log(copiedArray);

// 3) Obtener elementos del array
// const [user1, ...otherUsers] = names1;
// console.log(user1);
// console.log(otherUsers);
// console.log(names1);

// En funciones
// const logValues = (...args) => {
//   args.forEach((value) => console.log(value));
// };
// logValues("Hola", "Chau", "Como estas?", "No aparezco");

// En objetos

// 1) Copiar objetos
// const copiedPerson = { ...person1 };
// copiedPerson.city = "Rosario";
// console.log(copiedPerson);
// console.log(person1);

// Mergear objetos (vs opject.assign())
// const mergedObject = { ...person1, city: "Mendoza" };
// console.log(mergedObject);

// Otra forma de mergear
// const updateData = {
//   city: "Rosario",
//   age: 45,
// };
// const mergedObject = { ...person1, ...updateData };
// console.log(mergedObject);

// ********** FUNCTIONAL PROGRAMING **********

// const getFirstname = () => "Javier";
// console.log(getFirstname());
// // ---
// const getTwoNumber = () => 2;
// console.log(getTwoNumber() + 5);
// // ---
// const addMember = (currentMember, newMember) => {
//   const newMemberList = [...currentMember, newMember];
//   return newMemberList;
// };
// console.log(addMember(names1, "Marcelo"));

// FUNCIONES PURAS
// const sum = (a, b) => a + b; // porque dado los mismos valores, el resultado es el mismo.
// console.log(sum(2, 3));

// const getRandomNumber = () => Math.random();
// console.log(getRandomNumber()); // esta funcion no es pura.

// ********** HIGH ORDER FUNCTION **********

// const logResult = (result) => console.log(result);
// const alertResult = (result) => alert(result);
// const sum = (a, b, callback) => callback(a + b);

// logResult("Hola");
// sum(2, 6, logResult);
// sum(2, 1, alertResult);
