// ********** FUNCIONES **********

// Declaracion de la funcion
function sum(num1, num2) {
  console.log(num1 + num2); // num1 y num2 son parametros
}

// Invocar la funcion (llamar a la funcion)
sum(2, 3); // 2 y 3 son argumentos
sum(2, 6);
sum(5, 3);

// FUNCION DECLARACION (funcion convencional)

function sayHello() {
  return "Hola Mundo";
}
console.log(sayHello());

// FUNCION EXPLESION (definimos una constante como funcion y la llamamos a traves de esta)

const miFunExp = function (num1, num2) {
  return num1 + num2;
};

console.log(miFunExp(2, 5));

// ARROW FUNCTION (reemplazamos function por =>)

const miArrfun = (num3, num4) => {
  return num3 + num4;
};

console.log(miArrfun(2, 6));

// ARROW FUNCTION (cuando es una sola linea podemos quitar '{}')

const miArrfunImp = (num3, num4) => num3 + num4;

console.log(miArrfunImp(3, 6));

// ********** SCOPE **********

let isValid = false;

function validatePermission(role) {
  let validRole = "admin";
  let isValid = true;

  if (role === validRole) {
    let isValid = true;
    console.log("Dentro del if", isValid);
  } else {
    let isValid = true;
    console.log("Dentro del else", isValid);
  }

  console.log("Dentro de la funcion", isValid);
}

validatePermission("any");

console.log("A nivel global", isValid);

// ********** HOISTING **********

console.log(year);
//console.log(color);
//console.log(firstname);
//console.log(sayHello());
//console.log(sayGoodBye());
//console.log(sum(2, 5));

let color = "Red";
const firstname = "Diego";
var year = 2021;

function sayHello() {
  // tiene hosting (la eleva)
  return "Hola Mundo";
}

const sayGoodBye = function () {
  // no tiene hoisting (no la eleva)
  return "Bye Bye";
};

const sum = (num1, num2) => num1 + num2;

function sum(num1, num2) {
  return num1 + num2;
}

console.log(color, firstname, year);
