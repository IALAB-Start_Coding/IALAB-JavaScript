// 1)
/*
const findNumberType = (number) => {
  let message = "";
  const parsedNumber = +number;

  if (!parsedNumber && parsedNumber !== 0) {
    message = "El valor ingresado no es un numero";
    return message;
  }

  if (parsedNumber === 0) {
    message = "El numero ingresado es 0";
  } else if (parsedNumber > 0) {
    message = "El numero ingresado es positivo";
  } else {
    message = "El numero ingresado es negativo";
  }

  return message;
};

const chosenNumber = prompt("Ingresa un numero");

const response = findNumberType(chosenNumber);

alert(response);
*/
// 2)
/*
const isPrime = (number) => {
  const parsedNumber = +number;

  if (!parsedNumber && parsedNumber !== 0) {
    alert("El valor ingresado no es un numero");
    return;
  }

  if (parsedNumber < 2) return false;

  for (let i = 2; i < parsedNumber; i++) {
    if (parsedNumber % i === 0) return false;
  }

  return true;
};

const chosenNumber = prompt("Ingresa un numero");

const isNumberPrime = isPrime(chosenNumber);

if (isNumberPrime !== undefined) {
  if (isNumberPrime) alert("El numero es primo");
  else alert("El numero no es primo");
}
*/
// 3)
/*
// C a F (C x 9/5) + 32
// F a C (F -32) x 5/9

const convertTemperature = (temperature, target) => {
  const parsedTemp = +temperature;
  const parsedTarget = target.toLowerCase();

  if (!parsedTemp && parsedTemp !== 0) {
    alert("El valor ingresado no es valido");
    return;
  }

  if (parsedTarget === "celsius") {
    return ((parsedTemp - 32) * 5) / 9;
  } else if (parsedTarget === "faranheit") {
    return (parsedTemp * 9) / 5 + 32;
  } else {
    alert("El formato ingresado no es valido");
    return;
  }
};

const temperature = prompt("Ingresa la temperatura");
const target = prompt("Ingresa a que unidad de medida la queres convertir");

const response = convertTemperature(temperature, target);

if (response !== undefined) {
  alert(`La temperatura en ${target.toLowerCase()} es ${response}`);
}
*/
// 4)
/*
const calculateTotal = () => {
  let total = 0;
  let subTotal;

  do {
    subTotal = prompt("Ingresa el valor del producto");
    const parsedSubtotal = +subTotal;
    if (parsedSubtotal) {
      total += parsedSubtotal;
    } else {
      if (subTotal.toLowerCase() !== "total")
        alert("El valor ingresado no es correcto");
    }
  } while (subTotal.toLowerCase() !== "total");

  return total;
};

const response = calculateTotal();

alert(response);
*/
// Truthy y Falsy
/*
//FALSY: 0, null, undefined, NaN, '', "", ``, false

console.log(Boolean(false));
console.log(Boolean(null));
console.log(!!0);
console.log(!!"");
console.log(Boolean("Hola Mundo"));
console.log(Boolean(40));
console.log(!![]); // array
console.log(!!{}); // object

const firstName = prompt("Ingresa tu nombre");

if (+firstName) {
  console.log("esto es truthy", firstName);
} else {
  console.log("esto es falsy,", +firstName);
}
*/
