// 1)
const list = [
  "Lucas",
  "Matias",
  "Jose",
  "Pedro",
  "Martina",
  "Marcelo",
  "Esteban",
  "Marcela",
  "Martin",
];

console.log(`La cantidad de invitados es de ${list.length}`);

// 2)
console.log(`El ultimo invitado de la fiesta es ${list[list.length - 1]}`);

// 3)
list.push("Pedro");
console.log(list);

// 4.1)
const newList = list.filter((firstName) => firstName !== "Marcela");

// 4.2)
const listWithOrderNumber = newList.map(
  (firstName, index) => `${firstName}. Orden: ${index + 1}`
);
console.log(listWithOrderNumber);

// 5)
const totalDishes = newList.reduce((acummulator, currentValue) => {
  if (currentValue === "Lucas") {
    return acummulator + 3;
  } else if (currentValue === "Esteban" || currentValue === "Jose") {
    return acummulator + 1;
  } else {
    return acummulator + 4;
  }
}, 0);

console.log(totalDishes);

// 6)
const orderedNames = newList.sort();
console.log(orderedNames);
