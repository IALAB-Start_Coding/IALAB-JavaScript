// ********** ARRAYS **********

// Formas de declaracion de un array

const otraArray = new Array();
const miArray = ["Diego", "Armando", "Maradona"];

console.log(miArray);
console.log(miArray[0]);

// Logitud de un array
const otroNuevoArray = [["Juan", 30, {}], "Diego", 2, true, {}, () => {}];
console.log(otroNuevoArray);
console.log(otroNuevoArray.length);

// Acceder a los valores
console.log(`En ese casillero esta guardado ${miArray[0]}`);

// Actualizar un array
miArray[0] = "Ernesto";
console.log(miArray[0]);

// ARRAY MANIPULATION

const miArray = ["Pedro", "Javier", "Diego"];
const firstElement = miArray[0];
const lastElement = miArray[miArray.length - 1];

console.log(firstElement);
console.log(lastElement);

// saber el indice de un elemento
const element = "Javier";
const index = miArray.indexOf(element);
console.log(index);
// si no esta el elemento en el array, el valor devuelto es -1.

// Agregar un elemento al final del array
const newLength = miArray.push("Ernesto");
console.log(miArray);
console.log(newLength); // push retorna el nuevo largo del array.

// Remover un elemento del final del array
const removeElement = miArray.pop();
console.log(miArray);
console.log(removeElement); // notemos que pop retorna el elemento que saco. Nos puede servir o no guardarlo en una constantes.

// Unshift
miArray.unshift("Javier"); // mueve ese elemento al principio del array.
console.log(miArray);

// Shift
const removedElement = miArray.shift(); // remueve un elemento del principio del array.
console.log(removedElement);
console.log(miArray);

// Splice
console.log(miArray);
const valoreRemovido = miArray.splice(1, 0); // (desde, cantidad) // remueve en cantidad
console.log(valoreRemovido);
console.log(miArray);

// Slice
const newArray = miArray.slice(0, 2); // recorta un array.
console.log(miArray);
console.log(newArray);

// Iterar un array con For
for (let i = 0; i < miArray.length; i++) {
  console.log(miArray[i]);
}

// Iterar un array con ForeEach
miArray.forEach((item, index) => {
  console.log(index);
  console.log(item);
});

// Map

// SIN map
const myNewArray = [];

newArray.forEach((name, index) => {
  const newString = `Nombre: ${name}`;
  console.log(newString);
  myNewArray.push(newString);
});

console.log(myNewArray);

// CON map
const newArrayWithMap = miArray.map((name) => `Nombre: ${name}`); // hace un return del elemento que esta recorriendo.
console.log(newArrayWithMap);

// Filter
const filteredArray = newArray.filter((name) => name === "Javier"); // se queda con los elementos "Javier".
console.log(filteredArray);

// Find
const existingElement = newArray.find((name) => name === "Jaier"); // retorna el primer valor que cumpla la condicion.
console.log(existingElement);

if (existingElement) console.log("El nombre esta en la lizta");
else console.log("El nombre no esta en la lista");

// Sort
const orderedArray = miArray.sort();
console.log(orderedArray);
console.log(orderedArray.reverse());

// Reduce

// SIN reduce
const numbers = [10, 20, 30, 40];

let total = 0;
numbers.forEach((number) => (total += number));
console.log(total);

// CON reduce
const all = numbers.reduce(
  (acumulador, valorActual) => acumulador + valorActual, // retorna acumulador + valorActual a acumulador.
  0
); // 0 es el valor inicial de acumulador.

console.log(all);
